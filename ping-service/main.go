package main

import (
	"fmt"
	"log"
	"net"

	"github.com/derekparker/delve/service/api"

	"gitlab.com/pantomath-io/demo-grpc/api"
	//"gitlab.com/pantomath-io/demo-grpc/tree/master/api"
	//"proto/api"
	"google.golang.org/grpc"
)

//Main start a gRPC Server and waits for connection
func main() {
	//Create a listener on TCP port 7777
	lis, err := net.Listen("tcp", fmt.Printf(":%d", 7777))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	//create server instance
	s := api.Server{}

	//creat gRPC server object
	grpcServer := grpc.NewServer()

	//attach the ping service to the server
	api.RegisterPingServer(grpcServer, &s)

	//start the server
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("Failed to serve %s", err)
	}
}
