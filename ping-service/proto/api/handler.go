package api

import (
	//context "context"
	"log"

	"golang.org/x/net/context"
)

//Server represents the gRPC server
type Server struct {
}

//SayHello generates response to a ping request
func (s *Server) SayHello(ctx context.Context, in *PingMessage) (*PingMessage, error) {
	log.Printf("Received message %s", in.Greeting)
	return &PingMessage{Greeting: "bar"}, nil
}
