package main

import (
	"context"
	"log"
	"net"
	"sync"

	//pb "github.com/<YourUserName>/shippy-service-consignment/proto/consignment"
	//pb "gitlab.com/Prosp3r/shipping/consignment-service/proto/consignment"
	//pb "gitlab.com/Prosp3r/shipping/tree/master/consignment-service"
	//pb "gitlab.com/Prosp3r/shipping"
	pb "gitlab.com/Prosp3r/shipping/consignment-service/proto/consignment"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	port = ":50051"
)

type repository interface {
	Create(*pb.Consignment) (*pb.Consignment, error)
}

//Repository - Dummy repository. This simulates the use of a datastore
//of some kind. We'll replace this with a real implementation later on.
type Repository struct {
	mu          sync.RWMutex
	consignment []*pb.Consignment
}

//Create a new consignment
func (repo *Repository) Create(consignment *pb.Consignment) (*pb.Consignment, error) {
	repo.mu.Lock()
	updated := append(repo.consignment, consignment)
	repo.consignment = updated
	repo.mu.Unlock()
	return consignment, nil
}

//Service should implement all of the methods to satisfy the service
//we define in our protobuf definition. You can check the interface
//in the generated code itself for the exact method signatures etc
//to give a better idea
type service struct {
	repo repository
}

func (s *service) CreateConsignment(ctx context.Context, req *pb.Consignment) (*pb.Response, error) {

	//Save our consignment
	consignment, err := s.repo.Create(req)
	if err != nil {
		return nil, err
	}

	//Return matching the Response message we created in our protobuf definition.
	return &pb.Response{Created: true, Consignment: consignment}, nil
}

func main() {
	repo := &Repository{}

	//Set-up our RPC Server
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()

	//Register our service with the gRPC Server, this will tie our implementation
	//into the auto-generated interface code for our protobuf definition.
	pb.RegisterShippingServiceServer(s, &service{repo})

	//register reflection service on gRPC server.
	reflection.Register(s)

	log.Println("Running on port:", port)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}
