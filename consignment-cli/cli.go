package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"context"

	//pb "gitlab.com/Prosp3r/shipping/tree/master/consignment-service/proto/consignment/"
	pb "gitlab.com/Prosp3r/shipping/consignment-service/proto/consignment"
	//pb "github.com/EwanValentine/shippy-service-consignment/proto/consignment"
	"google.golang.org/grpc"
)

const (
	address         = "localhost:50051"
	defaultFilename = "consignment.json"
	//defaultFilename = "consignment.json"
)

func parseFile(file string) (*pb.Consignment, error) {
	var consignment *pb.Consignment
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	fmt.Print(data)
	json.Unmarshal(data, &consignment)
	return consignment, err
}

func main() {
	//Set up connection to the server
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Did not connect %v", err)
	}

	defer conn.Close()
	client := pb.NewShippingServiceClient(conn)

	//Contact the server and print out it's response
	file := defaultFilename
	if len(os.Args) > 1 {
		file = os.Args[1]
	}

	//log.Println("here now: ")
	consignment, err := parseFile(file)
	if err != nil {
		log.Fatalf("Could not parse file: %v", err)
	}

	r, err := client.CreateConsignment(context.Background(), consignment)
	if err != nil {
		log.Fatalf("Could not greet: %v", err)
	}
	log.Printf("Created: %t", r.Created)
}
